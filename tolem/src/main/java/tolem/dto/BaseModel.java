package tolem.dto;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BaseModel {
	
	public String getJson(){
		final GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation() ; 
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.setDateFormat("MMM dd, yyyy hh:mm:ss aa");
	    final Gson gson = builder.create() ;
	    return gson.toJson(this) ;
	}

}
