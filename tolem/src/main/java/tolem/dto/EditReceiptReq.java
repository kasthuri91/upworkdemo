package tolem.dto;

import java.sql.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditReceiptReq extends BaseModel {

	@SerializedName("id")
	@Expose
	private long id;
	
	@SerializedName("userId")
	@Expose
	private long userId;
	
	@SerializedName("productName")
	@Expose
	private String productName;
	
	@SerializedName("companyName")
	@Expose
	private String companyName;
	
	@SerializedName("category")
	@Expose
	private String category;
	
	@SerializedName("dateTime")
	@Expose
	private String dateTime;
	
	@SerializedName("fc")
	@Expose
	private String  fc;
	
	@SerializedName("rnm")
	@Expose
	private String  rnm;
	
	@SerializedName("payment")
	@Expose
	private String  payment;
	
	@SerializedName("total")
	@Expose
	private double  total;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getFc() {
		return fc;
	}

	public void setFc(String fc) {
		this.fc = fc;
	}

	public String getRnm() {
		return rnm;
	}

	public void setRnm(String rnm) {
		this.rnm = rnm;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
}
