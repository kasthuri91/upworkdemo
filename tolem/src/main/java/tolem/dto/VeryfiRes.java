package tolem.dto;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VeryfiRes extends BaseModel {

	@SerializedName("date")
	@Expose
	private Date date;
	
	@SerializedName("invoice_number")
	@Expose
	private String invoice_number;
	
	@SerializedName("products")
	@Expose
	private String products;
	
	@SerializedName("vendor")
	@Expose
	private String vendor;
	
	@SerializedName("subtotal")
	@Expose
	private double subtotal;
	
	@SerializedName("category")
	@Expose
	private String category;
	
	@SerializedName("phm")
	@Expose
	private String phm;
	
	@SerializedName("payment")
	@Expose
	private String payment;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getInvoice_number() {
		return invoice_number;
	}

	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}

	public String getProducts() {
		return products;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPhm() {
		return phm;
	}

	public void setPhm(String phm) {
		this.phm = phm;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}
}
