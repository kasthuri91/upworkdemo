package tolem.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditUserReq extends BaseModel {

	@SerializedName("userId")
	@Expose
	private long userId;
	
	@SerializedName("email")
	@Expose
	private String email;

	@SerializedName("pwd")
	@Expose
	private String pwd;
	
	@SerializedName("phone")
	@Expose
	private String phone;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	
}
