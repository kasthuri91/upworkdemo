package tolem.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author madhushika
 *
 */
public class SignUpRes extends BaseModel {

	@SerializedName("tolem_email")
	@Expose
	private String tolem_email;
	
	@SerializedName("userId")
	@Expose
	private long userId;
	
	@SerializedName("Status")
	@Expose
	private String Status;
	
	@SerializedName("name")
	@Expose
	private String name;
	
	@SerializedName("phone")
	@Expose
	private String phone;

	public String getTolem_email() {
		return tolem_email;
	}

	public void setTolem_email(String tolem_email) {
		this.tolem_email = tolem_email;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
