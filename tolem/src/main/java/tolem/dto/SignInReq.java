package tolem.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignInReq extends BaseModel {

	@SerializedName("email")
	@Expose
	private String email;

	@SerializedName("pwd")
	@Expose
	private String pwd;
	
	@SerializedName("token")
	@Expose
	private String token;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
