package tolem.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageRes extends BaseModel {

	@SerializedName("id")
	@Expose
	private long id;
	
	@SerializedName("userId")
	@Expose
	private long userId;
	
	@SerializedName("senderEmail")
	@Expose
	private String  senderEmail;
	
	@SerializedName("dateTime")
	@Expose
	private String  dateTime;
	
	@SerializedName("text")
	@Expose
	private String  text;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
