package tolem.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SingUpReq extends BaseModel{

	@SerializedName("email")
	@Expose
	private String email;

	@SerializedName("pwd")
	@Expose
	private String pwd;
	
	@SerializedName("name")
	@Expose
	private String name;
	
	@SerializedName("phone")
	@Expose
	private String phone;
	
	@SerializedName("token")
	@Expose
	private String token;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
