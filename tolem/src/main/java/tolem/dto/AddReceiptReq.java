package tolem.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddReceiptReq extends BaseModel {

	@SerializedName("userId")
	@Expose
	private long userId;
	
	@SerializedName("imageName")
	@Expose
	private String imageName;
	
	@SerializedName("base64")
	@Expose
	private String  base64;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	
}
