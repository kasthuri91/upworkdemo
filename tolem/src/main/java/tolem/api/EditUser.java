package tolem.api;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tolem.dto.EditUserReq;
import tolem.dto.SingUpReq;
import tolem.model.TolemMail;
import tolem.model.User;
import tolem.util.BaseDbManager;

/**
 * Servlet implementation class EditUser
 */
@WebServlet("/edituser")
public class EditUser extends BaseHandler {
	private static final long serialVersionUID = 1L;
      
	private BaseDbManager<User> dbm_user;
    
	public EditUser() {
        super();
        dbm_user = new BaseDbManager<User>(hibernateFactory);
    }

    public void init(ServletConfig config) throws ServletException {
		super.init(config);
		dbm_user = new BaseDbManager<User>(hibernateFactory);
	}
    
	@Override
	protected String processRequest(HttpServletRequest request, HttpServletResponse response, String strData)
			throws ServletException, IOException {
		
		return editUser(strData);
	}
	
	public String editUser(String strData) {
		
		EditUserReq req=null;
		
		try {
			req = gson.fromJson(strData, EditUserReq.class);

		} catch (Exception e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}

		if (req == null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}
		
		if(req.getUserId()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"UserId Can not be null\"}";
		}
		
		User user=null;
		
		try {
			user=dbm_user.get("tolem.model.User", req.getUserId());
		} catch (IOException e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}
		
		if (user==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"No User for the ID\"}";
		}
		
		if(req.getEmail()!=null && req.getEmail().length()>0) {
			
			String hql="FROM User WHERE email='"+req.getEmail()+"'";
			User exist_user=null;
			exist_user=dbm_user.get(hql);
			
			if(exist_user!=null) {
				return "{\"Status\":\"Fail\",\"Message\":\"Email Already Exists\"}";
			}
			user.setEmail(req.getEmail());
		}
		if(req.getPhone()!=null && req.getPhone().length()>0) {
			user.setPhone(req.getPhone());
		}
		if(req.getPwd()!=null && req.getPwd().length()>0) {
			user.setPwd(req.getPwd());
		}
		
		try {
			dbm_user.updateWithUniqueFields(user);
		} catch (Exception e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error \"}";
		}
		
		return "{\"Status\":\"Success\",\"Message\":\"User details updated successfully\"}"; 
	}

}
