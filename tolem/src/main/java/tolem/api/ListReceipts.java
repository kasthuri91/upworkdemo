package tolem.api;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tolem.dto.EditUserReq;
import tolem.dto.ListReceiptReq;
import tolem.dto.ReceiptRes;
import tolem.model.Receipt;
import tolem.model.User;
import tolem.util.BaseDbManager;

/**
 * Servlet implementation class ListReceipts
 */
@WebServlet("/listreceipts")
public class ListReceipts extends BaseHandler {
	private static final long serialVersionUID = 1L;
   
	private BaseDbManager<User> dbm_user;
	private BaseDbManager<Receipt> dbm_rec;
    public ListReceipts() {
        super();
        dbm_user = new BaseDbManager<User>(hibernateFactory);
        dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
    }
    
    public void init(ServletConfig config) throws ServletException {
		super.init(config);
		dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
		dbm_user = new BaseDbManager<User>(hibernateFactory);
	}

	@Override
	protected String processRequest(HttpServletRequest request, HttpServletResponse response, String strData)
			throws ServletException, IOException {
		
		return getList(strData);
	}

	public String getList(String strData) {
		
		ListReceiptReq req=null;
		
		try {
			req = gson.fromJson(strData, ListReceiptReq.class);

		} catch (Exception e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}

		if (req == null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}
		
		if(req.getUserId()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"UserId Can not be null\"}";
		}
		
		User user=null;
		
		try {
			user=dbm_user.get("tolem.model.User", req.getUserId());
		} catch (IOException e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}
		
		if (user==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"No User for the ID\"}";
		}
		
		String hql ="FROM Receipt WHERE userId="+req.getUserId();
		
		List<Receipt> ls =null;
		
		ls=dbm_rec.getRecords(hql, 0, 0);
		
		if(ls==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"No record found\"}";
		}
		return "{\"Status\":\"Success\",\"Array\":"+_generateReceiptJSArrayl(ls)+"}";
		
	}

	private String _generateReceiptJSArrayl(List<Receipt> ls) {

		String strJson = "";
		strJson = "[" + generateReceiptRes(ls.get(0));

		for (int i = 1; i < ls.size(); i++) {
			strJson = strJson + "," +generateReceiptRes(ls.get(i));

		}

		return strJson + "]";

	}
	
	private String generateReceiptRes(Receipt rec) {
		
		ReceiptRes res=new ReceiptRes();
		
		res.setCategory(rec.getCategory());
		res.setCompanyName(rec.getCompanyName());
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");  
		
		res.setDateTime(formatter.format(rec.getDateTime()));
		res.setFc(rec.getFc());
		res.setId(rec.getId());
		res.setLastEdit(formatter.format(rec.getLastEdit()));
		res.setProductName(rec.getProductName());
		res.setRnm(rec.getRnm());
		res.setTotal(rec.getTotal());
		res.setUserId(rec.getUserId());
	
		return res.getJson();
	}
	
}
