package tolem.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tolem.dto.EditReceiptReq;
import tolem.dto.ListReceiptReq;
import tolem.model.Receipt;
import tolem.model.User;
import tolem.util.BaseDbManager;

/**
 * Servlet implementation class CatergorySummery
 */
@WebServlet("/catsummery")
public class CategorySummery extends BaseHandler {
	private static final long serialVersionUID = 1L;
    
	private BaseDbManager<User> dbm_user;
	private BaseDbManager<Receipt> dbm_rec;
    public CategorySummery() {
        super();
        dbm_user = new BaseDbManager<User>(hibernateFactory);
        dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
    }
    
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
        dbm_user = new BaseDbManager<User>(hibernateFactory);
        dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
    }

	@Override
	protected String processRequest(HttpServletRequest request, HttpServletResponse response, String strData)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		return getCategorySummery(strData);
	}
	
	public String getCategorySummery(String strData) {
		
		ListReceiptReq req=null;
		
		try {
			req = gson.fromJson(strData, ListReceiptReq.class);

		} catch (Exception e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}
		
		if (req == null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}
		
		if(req.getUserId()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"UserId Can not be null\"}";
		}
		
		User user=null;
		
		try {
			user=dbm_user.get("tolem.model.User", req.getUserId());
		} catch (IOException e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}
		
		if (user==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"No User for the ID\"}";
		}
		
		String hql ="SELECT category,SUM(total) FROM Receipt WHERE userId="+req.getUserId()+" GROUP BY category";
		
		List ls=dbm_rec.getRecords(hql);
		
		String strRes =  "{\"Status\":\"Success\" ,"+ _generateCatJSArray(ls) + "}";
		
		return strRes;
		
		
	}
	
	private String _generateCatJSArray(List ls){

		Object[] elemFirst = (Object[]) ls.get(0);
		
		if (elemFirst == null ) {
			log.error("elemFirst == null || elemFirst.length == 0");
			return "{\"Status\":\"Fail\",\"Message\":\"No User for the ID\"}";
		}

		String strJson =  _generateResponseElem(elemFirst);

		for (int i = 1; i < ls.size(); i++) {

			Object[] elem = (Object[]) ls.get(i);

			if (elem == null ) {
				log.error("elem == null || elem.length ==0");
				return "{\"Status\":\"Fail\",\"Message\":\"No User for the ID\"}";
			}

			strJson = strJson + "," + _generateResponseElem(elem);
		}

		return strJson;
	}

	private String _generateResponseElem(Object[] row)  {

		System.out.println("row 1 : "+row[0].toString());
		
		return "\""+row[0].toString()+"\":"+row[1].toString();
	}


}
