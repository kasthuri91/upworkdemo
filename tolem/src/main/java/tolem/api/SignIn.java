package tolem.api;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tolem.dto.SignInReq;
import tolem.dto.SignUpRes;
import tolem.model.TolemMail;
import tolem.model.User;
import tolem.util.BaseDbManager;

/**
 * Servlet implementation class SignIn
 */
@WebServlet("/signin")
public class SignIn extends BaseHandler {
	private static final long serialVersionUID = 1L;
       
	private BaseDbManager<User> dbm_user;
	private BaseDbManager<TolemMail> dbm_tol;
	
    public SignIn() {
        super();
        dbm_user=new BaseDbManager<User>(hibernateFactory);
        dbm_tol = new BaseDbManager<TolemMail>(hibernateFactory);
    }

    public void init(ServletConfig config) throws ServletException {
		super.init(config);
		dbm_user=new BaseDbManager<User>(hibernateFactory);
		dbm_tol = new BaseDbManager<TolemMail>(hibernateFactory);
    }

	@Override
	protected String processRequest(HttpServletRequest request, HttpServletResponse response, String strData)
			throws ServletException, IOException {
		
		return getSignIn(strData);
	}

	public String getSignIn(String strData) {
		
		log.info("********* Inside SignIN *********");
		
		SignInReq req=null;
		try {
			req = gson.fromJson(strData, SignInReq.class);

		} catch (Exception e) {
			return "{\"Status\":\"Fail\"}";
		}
		if (req == null) {
			log.error("Sign in object is null.");
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Request\"}";
		}
		
		if(req.getEmail()==null || req.getEmail().length()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Request\"}";
		}

		if(req.getPwd()==null || req.getPwd().length()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Request\"}";
		}
		
		if(req.getToken()==null || req.getToken().length()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Request\"}";
		}
		
		String hql="FROM User WHERE email='"+req.getEmail()+"' AND pwd='"+req.getPwd()+"'";
		
		User user=null;
		user=dbm_user.get(hql);
		
		if(user==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Please SignUp\"}";
		}
		
		user.setToken(req.getToken());
		try {
			dbm_user.updateWithUniqueFields(user);
		} catch (Exception e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}
		
		hql="FROM TolemMail WHERE userId="+user.getId()+"";
		
		TolemMail t_mail=null;
		
		t_mail=dbm_tol.get(hql);
		
		if(t_mail==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Tolem email is null, Please sign Up again \"}";
		}
		
		SignUpRes res=new SignUpRes();
		
		res.setStatus("Success");
		res.setTolem_email(t_mail.getEmail());
		res.setUserId(user.getId());
		res.setName(user.getName());
		res.setPhone(user.getPhone());
		
		return res.getJson();
	}
}
