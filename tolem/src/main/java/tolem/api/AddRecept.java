package tolem.api;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tolem.dto.AddReceiptReq;
import tolem.dto.EditReceiptReq;
import tolem.dto.VeryfiRes;
import tolem.model.Receipt;
import tolem.model.User;
import tolem.util.BaseDbManager;
import tolem.util.ProcessImage;

/**
 * Servlet implementation class AddRecept
 */
@WebServlet("/addreceipt")
public class AddRecept extends BaseHandler {
	private static final long serialVersionUID = 1L;
      
	private BaseDbManager<User> dbm_user;
	private BaseDbManager<Receipt> dbm_rec;
    public AddRecept() {
        super();
        dbm_user = new BaseDbManager<User>(hibernateFactory);
        dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
    }

    public void init(ServletConfig config) throws ServletException {
		super.init(config);
		dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
		dbm_user = new BaseDbManager<User>(hibernateFactory);
	}
    
	@Override
	protected String processRequest(HttpServletRequest request, HttpServletResponse response, String strData)
			throws ServletException, IOException {
		
		return addReceipt(strData);
	}

	public String addReceipt(String strData) {
		
		AddReceiptReq req=null;
		
		try {
			req = gson.fromJson(strData, AddReceiptReq.class);

		} catch (Exception e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}

		if (req == null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}
		if(req.getUserId()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"UserId Can not be null\"}";
		}
		
		User user=null;
		
		try {
			user=dbm_user.get("tolem.model.User", req.getUserId());
		} catch (IOException e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}
		
		if (user==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"No User for the ID\"}";
		}
		
		if(req.getBase64()==null || req.getBase64().length()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid base64\"}";
		}
		if(req.getImageName()==null || req.getImageName().length()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Image name\"}";
		}
		
		String veryfiReq = "{\"file_name\":\"" + req.getImageName() + "\",\"file_data\":\"" + req.getBase64() + "\"}";
		String veryfiRes = "";
		try {
			veryfiRes = ProcessImage._connectVeryfi(veryfiReq);
			System.out.println(veryfiRes);
		} catch (Exception e) {
			System.out.println("Error in getting image data");
		}

		VeryfiRes res = null;

		try {
			res = gson.fromJson(veryfiRes, VeryfiRes.class);

		} catch (Exception e) {
			System.out.println("error in extracting json res image data : " + e);
		}

		if (res == null) {
			System.out.println("Null response from image process");
		}

		System.out.println("Processed Res : "+res.getJson());
		
		Receipt rec = new Receipt();

		rec.setCategory(res.getCategory());
		rec.setDateTime(res.getDate());
		rec.setLastEdit(new Date());
		rec.setProductName(res.getProducts());
		rec.setRnm(res.getPhm());
		rec.setFc(res.getInvoice_number());
		rec.setTotal(res.getSubtotal());
		rec.setUserId(req.getUserId());
		rec.setCompanyName(res.getVendor());
		rec.setPayment(res.getPayment());
		rec.setBase64(req.getBase64());
		dbm_rec.insert(rec);

		return "{\"Status\":\"Success\",\"Message\":\"Receipt was successfully added\"}";
	}
}
