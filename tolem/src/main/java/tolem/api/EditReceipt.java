package tolem.api;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tolem.dto.EditReceiptReq;
import tolem.dto.ListReceiptReq;
import tolem.model.Receipt;
import tolem.model.User;
import tolem.util.BaseDbManager;

/**
 * Servlet implementation class EditReceipt
 */
@WebServlet("/editreceipt")
public class EditReceipt extends BaseHandler {
	private static final long serialVersionUID = 1L;
    
	private BaseDbManager<User> dbm_user;
	private BaseDbManager<Receipt> dbm_rec;
    public EditReceipt() {
        super();
        dbm_user = new BaseDbManager<User>(hibernateFactory);
        dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
    }

    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
        dbm_user = new BaseDbManager<User>(hibernateFactory);
        dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
    }
    
	@Override
	protected String processRequest(HttpServletRequest request, HttpServletResponse response, String strData)
			throws ServletException, IOException {
		
		return getEditReceipt(strData);
	}

	public String getEditReceipt(String strData)  {
		
		EditReceiptReq req=null;
		
		try {
			req = gson.fromJson(strData, EditReceiptReq.class);

		} catch (Exception e) {
			System.out.println("Ex :"+e);
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}

		if (req == null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}
		
		if(req.getUserId()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"UserId Can not be null\"}";
		}
		
		User user=null;
		
		try {
			user=dbm_user.get("tolem.model.User", req.getUserId());
		} catch (IOException e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}
		
		if (user==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"No User for the ID\"}";
		}
		
		Receipt receipt=null;
		
		try {
			receipt=dbm_rec.get("tolem.model.Receipt", req.getId());
		} catch (IOException e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}
		
		if(req.getCompanyName()!=null && req.getCompanyName().length()>0) {
			receipt.setCompanyName(req.getCompanyName());
		}
		
		if(req.getProductName()!=null && req.getProductName().length()>0) {
			receipt.setProductName(req.getProductName());
		}
		
		if(req.getCategory()!=null && req.getCategory().length()>0) {
			receipt.setCategory(req.getCategory());
		}
		
		if(req.getFc()!=null && req.getFc().length()>0) {
			receipt.setFc(req.getFc());
		}
		
		if(req.getPayment()!=null && req.getPayment().length()>0) {
			receipt.setPayment(req.getPayment());
		}
		
		if(req.getRnm()!=null && req.getRnm().length()>0) {
			receipt.setRnm(req.getRnm());
		}
		
		if(req.getDateTime()!=null ) {
			Date d=null;
			try {
				d = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse(req.getDateTime());
			} catch (ParseException e) {
				System.out.println("er :"+e);
				return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
			}
			receipt.setDateTime(d);
		}
		
		if(req.getTotal()!=0  ) {
			receipt.setTotal(req.getTotal());
		}
		
		try {
			dbm_rec.updateWithUniqueFields(receipt);
		} catch (Exception e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error \"}";
		}
		
		return "{\"Status\":\"Success\",\"Message\":\"Receipt details updated successfully\"}"; 
	}
}
