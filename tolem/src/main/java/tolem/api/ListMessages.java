package tolem.api;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tolem.dto.ListReceiptReq;
import tolem.dto.MessageRes;
import tolem.model.Message;
import tolem.model.User;
import tolem.util.BaseDbManager;

/**
 * Servlet implementation class ListMessages
 */
@WebServlet("/listmessages")
public class ListMessages extends BaseHandler {
	private static final long serialVersionUID = 1L;
 
	private BaseDbManager<User> dbm_user;
	private BaseDbManager<Message> dbm_mes;
    public ListMessages() {
        super();
        dbm_user = new BaseDbManager<User>(hibernateFactory);
        dbm_mes = new BaseDbManager<Message>(hibernateFactory);
    }
    
    public void init(ServletConfig config) throws ServletException {
		super.init(config);
		dbm_mes = new BaseDbManager<Message>(hibernateFactory);
		dbm_user = new BaseDbManager<User>(hibernateFactory);
	}


	@Override
	protected String processRequest(HttpServletRequest request, HttpServletResponse response, String strData)
			throws ServletException, IOException {
		
		return getMessageList(strData);
	}

	public String getMessageList(String strData) {
		
		ListReceiptReq req=null;
		
		try {
			req = gson.fromJson(strData, ListReceiptReq.class);

		} catch (Exception e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}

		if (req == null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}
		
		if(req.getUserId()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"UserId Can not be null\"}";
		}
		
		User user=null;
		
		try {
			user=dbm_user.get("tolem.model.User", req.getUserId());
		} catch (IOException e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}
		
		if (user==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"No User for the ID\"}";
		}
		
		String hql ="FROM Message WHERE userId="+req.getUserId();
		
		List<Message> ls =null;
		
		ls=dbm_mes.getRecords(hql, 0, 0);
		
		if(ls==null) {
			return "{\"Status\":\"Fail\",\"Message\":\"No record found\"}";
		}
		return "{\"Status\":\"Success\",\"Array\":"+_generateMeseiptJSArrayl(ls)+"}";
		
	}

	private String _generateMeseiptJSArrayl(List<Message> ls) {

		String strJson = "";
		strJson = "[" + generateMessageRes(ls.get(0));

		for (int i = 1; i < ls.size(); i++) {
			strJson = strJson + "," +generateMessageRes(ls.get(i));

		}

		return strJson + "]";

	}
	
	private String generateMessageRes(Message msg) {
		
		MessageRes res=new MessageRes();
		
		res.setId(msg.getId());
		res.setSenderEmail(msg.getSenderEmail());
		res.setText(msg.getText());
		res.setUserId(msg.getUserId());
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");  
		res.setDateTime(formatter.format(msg.getDateTime()));
		
		return res.getJson();
	}
}
