package tolem.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import tolem.util.FetchEmailThread;

/**
 * Servlet implementation class UnitTst
 */
@WebServlet("/UnitTst")
public class UnitTst extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String releaseDate = "10th June  2020";
	
	private static final Logger logger = LogManager.getLogger();
    
    public UnitTst() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("Inside Unit test Component ,Received request with IP : "+request.getRemoteAddr());
		
		PrintWriter out = response.getWriter();
		
		out.println("*********************************************** ");
		out.println("Roll out date :" + releaseDate + "");
		
		out.println("*********************************************** ");
		
		try {
			FetchEmailThread t=new FetchEmailThread();
		} catch (InterruptedException e) {
			System.out.println("error in fetching email thread: "+e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	
	
}
