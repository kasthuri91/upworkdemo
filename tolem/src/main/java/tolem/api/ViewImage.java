package tolem.api;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tolem.model.Message;
import tolem.model.Receipt;
import tolem.util.BaseDbManager;

/**
 * Servlet implementation class TestImage
 */
@WebServlet("/viewimage")
public class ViewImage extends BaseHandler {

	private static final long serialVersionUID = 1L;
	
	private BaseDbManager<Message> dbm_mes;
	private BaseDbManager<Receipt> dbm_rec;
	

	public ViewImage() {
		super();
		dbm_mes = new BaseDbManager<Message>(hibernateFactory);
		dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
		
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		dbm_mes = new BaseDbManager<Message>(hibernateFactory);
		dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		int id = Integer.parseInt(request.getParameter("id"));
		String endpoint = request.getParameter("endpoint");
		System.out.println("id " + id);
		System.out.println("id " + endpoint);

		String base64="";
		
		if(endpoint!=null && endpoint.equalsIgnoreCase("message") ) {
			String hql = "FROM Message WHERE id=" + id;

			Message m = null;
			m = dbm_mes.get(hql);
			base64=m.getAttachement();
		}
		

		if(endpoint!=null && endpoint.equalsIgnoreCase("receipt") ) {
			String hql = "FROM Receipt WHERE id=" + id;

			Receipt r = null;
			r = dbm_rec.get(hql);
			base64=r.getBase64();
		}
		
		
		response.setContentType("image/jpeg");
		ServletOutputStream out;
		out = response.getOutputStream();
		
		byte[] imageByteArray = Base64.getDecoder().decode(base64);
		InputStream is = new ByteArrayInputStream(imageByteArray);

		BufferedOutputStream bout = new BufferedOutputStream(out);
		int ch = 0;
		;
		while ((ch = is.read()) != -1) {
			bout.write(ch);
		}

		is.close();
		bout.close();
		out.close();
	}

	@Override
	protected String processRequest(HttpServletRequest request, HttpServletResponse response, String strData)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		return null;
	}
}