package tolem.api;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import tolem.util.FetchEmailThread;
import tolem.util.HibernateUtil;

public abstract class BaseHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected static Logger log;
	protected static SessionFactory hibernateFactory;
	protected static Gson gson;
	
	public BaseHandler() {
		try {
			gson = new GsonBuilder().create();
			_setLogger();
			hibernateFactory = HibernateUtil.getSessionFactory();
			
		} catch (Exception e) {
			System.out
					.println("Error in init BaseHandler constructor");
			System.out.println(e.toString());
		}
	}

	public void init(ServletConfig config) throws ServletException {
		
		_setLogger();

		try {
			gson = new GsonBuilder().create();
			hibernateFactory = HibernateUtil.getSessionFactory();
		} catch (Exception e) {
			throw new ServletException("Error in init BaseHandler", e);
		}
		
		super.init(config);
	}

	
	private void _setLogger() {
		log = LogManager.getLogger(this.getClass());
	}
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		warnLogWithSignature(request, "** Received GET Request **");

		response.sendError(HttpServletResponse.SC_FORBIDDEN);
	}

	public boolean isNullOrEmptyString(String str) {
		if (str == null) {
			return true;
		}

		if (str.length() == 0) {
			return true;
		}

		return false;
	}

	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/json");
		infoLogWithSignature(request, "Received post Request");

		try {
		
			String strData = readRawBody(request, response);

			if (strData != null) {
				log.info(strData);
			} else {
				log.info("Str data is null");
			}

			String strRes = processRequest(request, response, strData);

			PrintWriter out = response.getWriter();
			if (strRes != null) {
				out.println(strRes);
			}

			infoLogWithSignature(request, "end of processing post Request");

			if (strData != null) {
				log.info(strRes);
			} else {
				log.info("strRes is null");
			}

		} catch (Exception e) {
			response.sendError(response.SC_BAD_REQUEST, "Invalid Request");
		} finally {
			response.setHeader("Cache-Control",
					"no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
		}
	}
	protected String readRawBody(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{

		StringBuffer jb = new StringBuffer();
		String line = null;

		BufferedReader reader = request.getReader();

		while ((line = reader.readLine()) != null) {
			jb.append(line);
		}

		if (jb.toString().trim().length() == 0) {
			System.out.println("Empty body");
		}

		String res = jb.toString().trim();
		log.info(" request : " + res);

		return res;
	}

	abstract protected String processRequest(HttpServletRequest request,
			HttpServletResponse response, String strData)
			throws ServletException, IOException;

	protected void infoLogWithSignature(HttpServletRequest request,
			String strlog) {
		log.info("IP:" + request.getRemoteAddr() + " - " + strlog);
	}

	protected void warnLogWithSignature(HttpServletRequest request,
			String strlog) {
		log.warn("IP:" + request.getRemoteAddr() + " - " + strlog);
	}

}
