package tolem.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;

import tolem.dto.SignUpRes;
import tolem.dto.SingUpReq;
import tolem.model.TolemMail;
import tolem.model.User;
import tolem.util.BaseDbManager;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/signup")
public class SignUp extends BaseHandler {
	private static final long serialVersionUID = 1L;

	private BaseDbManager<TolemMail> dbm_tol;
	private BaseDbManager<User> dbm_user;

	public SignUp() {
		super();
		dbm_user = new BaseDbManager<User>(hibernateFactory);
		dbm_tol = new BaseDbManager<TolemMail>(hibernateFactory);
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		dbm_user = new BaseDbManager<User>(hibernateFactory);
		dbm_tol = new BaseDbManager<TolemMail>(hibernateFactory);
	}

	@Override
	protected String processRequest(HttpServletRequest request, HttpServletResponse response, String strData)
			throws ServletException, IOException {

		return getSugnUp(strData);
	}

	public String getSugnUp(String strData) {

		log.info("********** Inside SignUp **********");

		SingUpReq req = null;

		try {
			req = gson.fromJson(strData, SingUpReq.class);

		} catch (Exception e) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}

		if (req == null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Data\"}";
		}

		if (req.getEmail()==null || req.getEmail().length() == 0) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Email\"}";
		}

		if (req.getPwd()==null || req.getPwd().length() == 0) {
			return "{\"Status\":\"Fail\",\"Message\":\"Invalid Password\"}";
		}

		User user = new User();
		user.setEmail(req.getEmail());
		user.setPwd(req.getPwd());
		user.setName(req.getName());
		user.setPhone(req.getPhone());
		user.setStatus("active");

		String hql = "FROM TolemMail WHERE userId=0 ";
		List<TolemMail> ls =null;
		try {
			 ls=dbm_tol.getRecords(hql, 0, 1);
		} catch (Exception e) {
			System.out.println("Err :" + e);
			log.info("Error in getting Tol_emai : "+e);
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}
		
		if(ls==null || ls.size()==0) {
			return "{\"Status\":\"Fail\",\"Message\":\"Tolem Domain Emails Full\"}";
		}
		
		hql="FROM User WHERE email='"+req.getEmail()+"'";
		User exist_user=null;
		exist_user=dbm_user.get(hql);
		
		if(exist_user!=null) {
			return "{\"Status\":\"Fail\",\"Message\":\"Email Already Exists\"}";
		}
		
		long id=0;
		try {
			id=dbm_user.insert(user);

		} catch (HibernateException he) {
			return "{\"Status\":\"Fail\",\"Message\":\"Internal Error\"}";
		}


		TolemMail t_email=ls.get(0);
		
		t_email.setUserId(id);
		
		try {
			dbm_tol.updateWithUniqueFields(t_email);
		} catch (Exception e) {
			System.out.println("Err :" + e);
			log.info("Error in updating Tol_emai : "+e);
			return "{\"Status\":\"Fail\",\"Status\":\"Internal Error\"}";
		}
	
		SignUpRes res=new SignUpRes();
		
		res.setStatus("Success");
		res.setTolem_email(t_email.getEmail());
		res.setUserId(id);
		
		return res.getJson();
	}

}
