package tolem.util;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import tolem.api.MPDServletConList;

public class BaseDbManager<T> {

	protected static Logger log;

	protected SessionFactory factory;

	public BaseDbManager(SessionFactory f) {
		factory = f;
		log = LogManager.getLogger(MPDServletConList.class);
	}

	public long insert(T obj) throws HibernateException {
		log.info("inside insert(T obj)");
		long res = 0;
		Session session = factory.openSession();
		Long dbId = null;

		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			
			dbId = (Long) session.save(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("Received exception in last insert tx.");

			if (tx != null) {
				try {
					log.info("Rolling back the last insert.");
					tx.rollback();
				} catch (HibernateException e1) {
					log.error("Error in roll back.");
				}
			}

			try {
				if (session != null) {
					if (session.isOpen()) {
						session.close();
					}
				}
			} catch (HibernateException e2) {
				log.info("Error in closing session. " + e2.toString());
				System.out.println("Errr in insert : " + e2.toString());
			}

			throw e;
		}

		res = dbId.longValue();

		try {
			if (session != null) {
				if (session.isOpen()) {
					session.close();
				}
			}
		} catch (HibernateException e) {
			log.error("Error in closing session. " + e.toString());
		}

		return res;
	}

	public List<T> getRecords(String hSql, int start, int size) {

		log.info("List<T> getRecords(String hSql, int start, int size)");

		System.out.println("Inside get records :");
		List<T> records = null;
		Session session = null;

		try {

			session = factory.openSession();
			Transaction tx = session.beginTransaction();
			Query query = session.createQuery(hSql);
			query.setFirstResult(start);
			query.setMaxResults(size);
			// List<T> records = query.list() ;
			records = query.list();
			tx.commit();

			// session.close();
		} catch (HibernateException e) {
			log.error("HibernateException : " + e.toString());
		}

		try {
			if (session != null) {
				if (session.isOpen()) {
					session.close();
				}

			}

		} catch (HibernateException e) {
			log.error("Error in closing session. " + e.toString());
		}

		return records;
	}

	public List<T> getRecords(String hSql) {

		log.info("List<T> getRecords(String hSql, int start, int size)");

		System.out.println("Inside get records :");
		List<T> records = null;
		Session session = null;

		try {

			session = factory.openSession();
			Transaction tx = session.beginTransaction();
			Query query = session.createQuery(hSql);
			
			// List<T> records = query.list() ;
			records = query.list();
			tx.commit();

			// session.close();
		} catch (HibernateException e) {
			log.error("HibernateException : " + e.toString());
			System.out.println("Hib : "+e);
		}

		try {
			if (session != null) {
				if (session.isOpen()) {
					session.close();
				}

			}

		} catch (HibernateException e) {
			log.error("Error in closing session. " + e.toString());
			System.out.println("Hib 2: "+e);
		}

		return records;
	}
	
	public T get(String hsql) {

		log.info("T get(String entity, Long id)");

		Session session = null;

		T obj = null;

		try {

			session = factory.openSession();
			Transaction tx = session.beginTransaction();
			Query query = session.createQuery(hsql);

			obj = (T) query.uniqueResult();

			tx.commit();

		} catch (HibernateException e) {
			log.error("Error in closing session. " + e.toString());
		}

		try {
			if (session != null) {
				if (session.isOpen()) {
					session.close();
				}

			}

		} catch (HibernateException e) {
			log.error("Error in closing session. " + e.toString());
		}

		return obj;
	}

	public void updateWithUniqueFields(T obj) throws Exception {
		log.info("updateWithUniqueFields(T obj)");

		Session session = null;

		try {
			session = factory.openSession();
		} catch (HibernateException e) {
			log.error("Error in creating session. " + e.toString());
			throw new Exception("Error Updating ,error : "+e);
		}

		if (session == null) {
			log.error("Session is null.");
			throw new Exception("Error Updating ,error : ");
		}

		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("Received exception in last update tx.");
			if (tx != null) {
				try {
					log.info("Rolling back the last update.");
					tx.rollback();
				} catch (HibernateException e1) {
					log.error("Error in roll back.");
				}

			}

			try {
				if (session != null) {
					if (session.isOpen()) {
						session.close();
					}

				}
			} catch (HibernateException e2) {
				log.info("Error in closing session. " + e2.toString());
			}

			throw e;

		}

		try {
			if (session != null) {
				if (session.isOpen()) {
					session.close();
				}
			}
		} catch (HibernateException e2) {
			log.error("Error in closing session. " + e2.toString());
		}
	}
	public T get(String entity, Long id) throws IOException{

		log.info("T get(String entity, Long id)");

		Session session = null;

		T obj = null;

		try {

			session = factory.openSession();
			Transaction tx = session.beginTransaction();
			obj = (T) session.get(entity, id);
			tx.commit();

			// session.close();
		} catch (HibernateException e) {
			log.error("Error in closing session. " + e.toString());
		}

		try {
			if (session != null) {
				if (session.isOpen()) {
					session.close();
				}

			}

		} catch (HibernateException e) {
			log.error("Error in closing session. " + e.toString());
		}

		return obj;
	}
	
}