package tolem.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;

import org.hibernate.SessionFactory;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import tolem.dto.VeryfiRes;
import tolem.model.Message;
import tolem.model.Receipt;
import tolem.model.TolemMail;

public class FetchNewEmail {

	protected static SessionFactory hibernateFactory;
	private BaseDbManager<Message> dbm_mes;
	private BaseDbManager<TolemMail> dbm_tol;
	private BaseDbManager<Receipt> dbm_rec;

	protected static Gson gson;

	public FetchNewEmail() {
		hibernateFactory = HibernateUtil.getSessionFactory();
		dbm_mes = new BaseDbManager<Message>(hibernateFactory);
		dbm_tol = new BaseDbManager<TolemMail>(hibernateFactory);
		dbm_rec = new BaseDbManager<Receipt>(hibernateFactory);
		gson = new GsonBuilder().create();
	}

	public void getEmail() throws Exception {

		String hql = "FROM TolemMail ";
		List<TolemMail> ls = null;

		try {
			ls = dbm_tol.getRecords(hql);
		} catch (Exception e) {
			System.out.println("error in : " + e);
		}

		if (ls == null) {
			System.out.println("tolem_email is null");
		}

		int count = ls.size();
		
		for (int i = 0; i < ls.size(); i++) {

			TolemMail tl_email = ls.get(i);
			if(tl_email.getUserId()!=0) {
				this.fetchEmail(tl_email.getUserId(), tl_email.getEmail(), tl_email.getPwd());
			}
			
		}

	}
	
	public Date getLastMsgTime(long userId) {
		String hql = "FROM Message WHERE userId="+userId+" ORDER BY dateTime DESC";
		List<Message> ls = null;

		try {
			ls = dbm_mes.getRecords(hql,0,1);
		} catch (Exception e) {
			System.out.println("error in : " + e);
		}
		if(ls==null || ls.size()==0) {
			return null;
		}
		Message lastMsg=ls.get(0);
		
		return lastMsg.getDateTime();

	}

	private void fetchEmail(long userId, String username, String pwd) throws ParseException, Exception {

		Properties properties = new Properties();
		properties.put("mail.pop3.host", "pop.gmail.com");
		properties.put("mail.pop3.port", "995");

		properties.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.setProperty("mail.pop3.socketFactory.fallback", "false");
		properties.setProperty("mail.pop3.socketFactory.port", String.valueOf("995"));

		Session session = Session.getDefaultInstance(properties);

		try {
			Store store = session.getStore("pop3");
			store.connect(username, pwd);
			Folder folderInbox = store.getFolder("INBOX");
			folderInbox.open(Folder.READ_ONLY);
			
			String body = "";
			
			//Flags seen = new Flags(Flags.Flag.SEEN);
		    //FlagTerm unseenFlagTerm = new FlagTerm(seen, true);
		    System.out.println("00000001 ");
		   
			javax.mail.Message[] arrayMessages = folderInbox.getMessages();
		    //javax.mail.Message[] arrayMessages = folderInbox.search(unseenFlagTerm);

			for (int i = 0; i < arrayMessages.length; i++) {
				System.out.println("00000002 ");
				javax.mail.Message message = arrayMessages[i];
				Address[] fromAddress = message.getFrom();
				String from = fromAddress[0].toString();
				
				String contentType = message.getContentType();
				
				String attachFiles = "";
				String base64 = "";
				String fileName = "";
				if (contentType.contains("multipart")) {
					
					Multipart multiPart = (Multipart) message.getContent();

					body = getTextFromMimeMultipart(multiPart);

					int numberOfParts = multiPart.getCount();

					for (int partCount = 0; partCount < numberOfParts; partCount++) {
						MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
						if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {

							fileName = part.getFileName();
							attachFiles += fileName + ", ";

							String extension = fileName.substring(fileName.lastIndexOf("."));

							if (extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpg")
									|| extension.equalsIgnoreCase(".jpeg")) {

								try (InputStream imageInFile = part.getInputStream()) {

									byte[] imageBytes = new byte[(int) part.getSize()];
									imageInFile.read(imageBytes, 0, imageBytes.length);
									imageInFile.close();
									base64 = Base64.getEncoder().encodeToString(imageBytes);

								} catch (FileNotFoundException e) {
									System.out.println("Image not found" + e);
								} catch (IOException ioe) {
									System.out.println("Exception while reading the Image " + ioe);
								}
							}
							
						}

					}

					if (attachFiles.length() > 1) {
						attachFiles = attachFiles.substring(0, attachFiles.length() - 2);
					}
				}
				
				Date endDate=this.getLastMsgTime(userId);
				
				
				if(this.getLastMsgTime(userId)==null) {
					endDate=new Date();
				}
				System.out.println("******************");
				System.out.println("UserId "+userId);
				System.out.println("mdate : "+this.getLastMsgTime(userId));
				System.out.println("Email date : "+message.getSentDate());
				System.out.println("******************");
				
				if (base64.length() > 0 && message.getSentDate().compareTo(endDate)>0) {
					System.out.println("00000004 ");
					this.getLastMsgTime(userId);
					
					Message m = new Message();

					m.setAttachement(base64);
					m.setDateTime(message.getSentDate());
					m.setLastEdit(new Date());

					String sender = "";

					Pattern p = Pattern.compile("\\<(.*?)\\>");
					Matcher match = p.matcher(from);

					while (match.find()) {
						sender = match.group(1);
					}

					m.setSenderEmail(sender);
					m.setText(body);
					m.setUserId(userId);

					dbm_mes.insert(m);
					System.out.println("File name : " + fileName);

					String veryfiReq = "{\"file_name\":\"" + fileName + "\",\"file_data\":\"" + base64 + "\"}";
					String veryfiRes = "";
					try {
						veryfiRes = ProcessImage._connectVeryfi(veryfiReq);
						System.out.println(veryfiRes);
					} catch (Exception e) {
						System.out.println("Error in getting image data");
					}

					VeryfiRes res = null;

					try {
						res = gson.fromJson(veryfiRes, VeryfiRes.class);

					} catch (Exception e) {
						System.out.println("error in extracting json res image data : " + e);
					}

					if (res == null) {
						System.out.println("Null response from image process");
					}

					System.out.println("Processed Res : "+res.getJson());
					
					Receipt rec = new Receipt();

					rec.setCategory(res.getCategory());
					rec.setDateTime(res.getDate());
					rec.setLastEdit(new Date());
					rec.setProductName(res.getProducts());
					rec.setRnm(res.getPhm());
					rec.setFc(res.getInvoice_number());
					rec.setTotal(res.getSubtotal());
					rec.setUserId(userId);
					rec.setCompanyName(res.getVendor());
					rec.setPayment(res.getPayment());
					rec.setBase64(base64);
					dbm_rec.insert(rec);

				}
			}

			folderInbox.close(true);
			store.close();
		} catch (NoSuchProviderException ex) {
			System.out.println("No provider for pop3 : "+ex);
			ex.printStackTrace();
		} catch (MessagingException ex) {
			System.out.println("Could not connect to the message store : "+ex);
			ex.printStackTrace();
		} catch (IOException ex) {
			System.out.println("Ioexception : "+ex);
		}

	}

	private String getTextFromMimeMultipart(Multipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break;
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}

}
