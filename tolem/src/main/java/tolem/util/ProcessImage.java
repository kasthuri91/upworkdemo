package tolem.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import tolem.dto.VeryfiRes;
import tolem.env.SysConfig;
import tolem.model.Receipt;

public class ProcessImage {

	protected static Gson gson;

	public static String _connectVeryfi(String json) throws Exception {

		String url = SysConfig.BASE_URL;
		HttpsURLConnection httpClient = (HttpsURLConnection) new URL(url).openConnection();

		httpClient.setRequestMethod("POST");
		httpClient.setRequestProperty("AUTHORIZATION", SysConfig.API_KEY);
		httpClient.setRequestProperty("CLIENT-ID", SysConfig.CLIENT_ID);
		httpClient.setRequestProperty("Content-Type", "application/json");
		httpClient.setRequestProperty("Content-Length", String.valueOf(json.length()));
		httpClient.setDoOutput(true);

		// Send post request
		httpClient.setDoOutput(true);
		try (DataOutputStream wr = new DataOutputStream(httpClient.getOutputStream())) {
			wr.write(json.getBytes());
			wr.flush();
		} catch (Exception e) {
			System.out.println("Error  : " + e);
			return null;
		}

		int responseCode = httpClient.getResponseCode();

		System.out.println("ResponseCode : " + responseCode);

		try (BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()))) {
			String line;
			StringBuilder response = new StringBuilder();

			while ((line = in.readLine()) != null) {
				response.append(line);
			}
			System.out.println("Veryfi Response : " + response.toString());

			String strRes = response.toString();

			return getData(strRes);
		} catch (Exception e) {
			System.out.println("Error in Veryfi response");
		}
		return null;
	}

	public static String getData(String d) {

		gson = new GsonBuilder().create();

		String phm = "";
		String fc = "";
		String productName = "";
		String payment = "";
		double total = 0;
		try {
			JSONParser parser = new JSONParser();

			JSONObject jsonObject = (JSONObject) parser.parse(d.toString());

			String category = (String) jsonObject.get("category");
			String date = (String) jsonObject.get("date");
			String invoice_no = (String) jsonObject.get("invoice_number");
			double subtotal = (double) jsonObject.get("subtotal");

			JSONObject jsonVender = (JSONObject) jsonObject.get("vendor");

			String vender = (String) jsonVender.get("name");

			if (subtotal == 0) {
				subtotal = (double) jsonObject.get("total");
			}
			
			JSONArray items=(JSONArray) jsonObject.get("line_items");
			for(int i=0;i<items.size();i++) {
				
				JSONObject product = (JSONObject) items.get(i);
				
				double quantity=(double) product.get("quantity");
				double amount=(double) product.get("total");
				
				if(quantity!=0 && amount !=0 ) {
					
					String name=(String) product.get("description");
					productName=productName+","+name.substring(name.lastIndexOf("\n") + 1);
				}
				
			}
			
			String ocr_text = (String) jsonObject.get("ocr_text");

			String[] arrOfStr = ocr_text.split("\n");

			for (int i = 0; i < arrOfStr.length; i++) {
				String data = arrOfStr[i];
				if (data.contains("PHм") || data.contains("PHK") || data.contains("PHM") || data.contains("РНК")
						|| data.contains("PHH") || data.contains("ККМ КГД")) {

					phm = data.replaceAll("\\D+", "");

				}
				if (data.contains("ПРО") || data.contains("фискальный признак")) {
					fc = data.replaceAll("\\D+", "");
				}
				if (data.contains("ИТОГ")) {
					String tot = "";
					if (data.contains(".")) {
						tot = data.replaceAll("\\D+", "");
						tot = tot.substring(0, tot.length() - 2);
					} else {
						tot = data.replaceAll("\\D+", "");
					}
					total = Double.parseDouble(tot);
				}
				if (data.contains("Наличные")) {
					payment = "cash";
				}
				if (data.contains("Банковская карта")) {
					payment = "cashless";
				}

			}

			if (subtotal < total) {
				subtotal = total;
			}
			if (invoice_no.length() == 0 || invoice_no == null) {
				invoice_no = fc;
			}

			Date dateFormated = null;

			dateFormated = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date);

			VeryfiRes res = new VeryfiRes();

			res.setCategory(category);
			res.setDate(dateFormated);
			res.setInvoice_number(invoice_no);
			res.setPayment(payment);
			res.setPhm(phm);
			res.setProducts(productName);
			res.setSubtotal(subtotal);
			res.setVendor(vender);

			return res.getJson();

		} catch (Exception e) {
			System.out.println("error in extracting vryfi res : " + e);
		}

		return null;
		
	}
	
	public static void main(String args[]) {
		
		System.out.println(ProcessImage.getData(SysConfig.veryfires));
		
	}

}
