package tolem.util;

import java.util.Timer;
import java.util.TimerTask;

import org.json.simple.parser.ParseException;

public class FetchEmailThread extends TimerTask {

	public FetchEmailThread() throws InterruptedException {
		//new Timer().schedule(this, 0, 600000);
		new Timer().schedule(this, 0, 50000);
	}

	@Override
	public void run() {
		FetchNewEmail f=new FetchNewEmail();
		try {
			f.getEmail();
		} catch (Exception e) {
			System.out.println("Error in starting thread");
		}
	}
}
