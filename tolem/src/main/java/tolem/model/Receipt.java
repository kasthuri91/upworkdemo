package tolem.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "receipt")
public class Receipt {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;

	@Column(name = "userId")
	private long  userId;
	
	@Column(name = "total")
	private double  total;
	
	@Column(name = "dateTime")
	private Date  dateTime;
	
	@Column(name = "fc")
	private String  fc;
	
	@Column(name = "rnm")
	private String  rnm;
	
	@Column(name = "productName")
	private String  productName;
	
	@Column(name = "companyName")
	private String  companyName;
	
	@Column(name = "category")
	private String  category;
	
	@Column(name = "lastEdit")
	private Date  lastEdit;

	@Column(name = "payment")
	private String  payment;
	
	@Column(name = "base64")
	private String  base64;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getFc() {
		return fc;
	}

	public void setFc(String fc) {
		this.fc = fc;
	}

	public String getRnm() {
		return rnm;
	}

	public void setRnm(String rnm) {
		this.rnm = rnm;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getLastEdit() {
		return lastEdit;
	}

	public void setLastEdit(Date lastEdit) {
		this.lastEdit = lastEdit;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}
	
}
